import java.util.Random;

public class CalculatorTest {
    public  static void main(String[] args) {

        for (int i = 0; i < 10000; i++) {
            String rawExp = generateExpression();
            Double result = new Calculator().eval(rawExp);
            if (result != null) {
                System.out.println(i + ") " + rawExp + " = " + result);
            }
        }


    }

    private static String generateExpression() {
        String signs = "()+-*/^";

        int expLength = new Random().nextInt(6) + 10;

        String expression = "";
        for (int i = 0; i < expLength; i++) {
            boolean isNum = new Random().nextBoolean();
            if (isNum) {
                expression += new Random().nextInt(100);
            } else {
                expression += signs.charAt(new Random().nextInt(signs.length()));
            }

        }

        return expression;
    }

}
