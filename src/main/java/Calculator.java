import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Calculator {

	public Double eval(String rawExp) {


		// remove blanks
		rawExp = rawExp.replace(" ", "");

		// split to tokens
		List tokens;
		try {
			tokens = parseExpression(rawExp);
		} catch (NumberFormatException e) {
			//System.out.println("Invalid expression");
			return null;
		}

		// fix assumptions
		fixExpression(tokens);

		// run calculation
		try {
			calculate(tokens);
		} catch (ArithmeticException e) {
			//System.out.println("Calculation error");
			return null;
		} catch (Exception e) {
			//System.out.println("Invalid expression");
			return null;
		}

		// interpret results
		Double result;
		if (tokens.size() == 1 && tokens.get(0) instanceof Double) {
			// success
			result = (Double) tokens.get(0);
		} else {
			//System.out.println("Invalid expression");
			result = null;
		}

		return result;
	}

	private static List parseExpression(String rawExp) {
		// resulting list of tokens
		List tokens = new ArrayList();
		// list of all possible signs
		List signs = Arrays.asList("(", ")", "+", "-", "*", "/", "^");

		// remaining expression to parse
		String exp = rawExp;
		// run while exp is not empty
		while (exp.length() > 0) {
			// locate index of first sign
			int signIndex = -1;
			for (int i = 0; i < exp.length(); i++) {
				if (signs.contains("" + exp.charAt(i))) {
					signIndex = i;
					break;
				}
			}
			// extract number before located sign
			if (signIndex > 0 || signIndex < 0) {
				// if sign located then number before sign or entire remaining expression is a number itself
				String num = signIndex > 0 ? exp.substring(0, signIndex) : exp;
				// try to parse number
				tokens.add(Double.parseDouble(num));
			}
			// extract located sign
			if (signIndex >= 0) {
				String sign = "" + exp.charAt(signIndex);
				tokens.add(sign);
			}
			// modify remaining expression: everything after sign or empty
			exp = signIndex < 0 ? "" : exp.substring(signIndex + 1);
		}

		return tokens;
	}

	private static void fixExpression(List tokens) {
		// assumption 1: - before number
		fixNegativeNumbers(tokens);

		// assumption 1: * before (
		fixMultiplicationSigns(tokens);
	}

	private static void fixNegativeNumbers(List tokens) {
		// valid tokens before '-'
		List signs = Arrays.asList("(", "*", "/", "^", null);

		while(true) {
			// is anything fixed this time
			boolean fixed = false;
			// go through all tokens
			for (int i = 0; i < tokens.size(); i++) {
				Object token = tokens.get(i);
				// if '-' sign found
				if ("-".equals(token)) {
					// get surrounding tokens
					Object before = i - 1 < 0 ? null : tokens.get(i - 1);
					Object after = i + 1 >= tokens.size() ? null : tokens.get(i + 1);
					if (signs.contains(before) && after instanceof Double) {
						// replace number after
						after = - ((Double) after);
						tokens.set(i + 1, after);
						// remove current '-' sign
						tokens.remove(i);
						// stop current processing
						fixed = true;
						break;
					}
				}
			}

			// if nothing was fixed this time then fixing is over
			if (!fixed) {
				break;
			}
		}
	}

	private static void fixMultiplicationSigns(List tokens) {
		// valid tokens before '-'
		List signs = Arrays.asList(")");

		while(true) {
			// is anything fixed this time
			boolean fixed = false;
			// go through all tokens
			for (int i = 0; i < tokens.size(); i++) {
				Object token = tokens.get(i);
				// if '(' sign found
				if ("(".equals(token)) {
					// get previous token
					Object before = i - 1 < 0 ? null : tokens.get(i - 1);
					if (signs.contains(before) || before instanceof Double) {
						// insert '*' before current token
						tokens.add(i, "*");
						// stop current processing
						fixed = true;
						break;
					}
				}
			}

			// if nothing was fixed this time then fixing is over
			if (!fixed) {
				break;
			}
		}
	}

	private static void calculate(List tokens) {
		while (true) {
			// rule 1: '^' between numbers
			if (applyPowerRule(tokens)) {
				//System.out.println(tokens);
				continue;
			}
			// rule 2: '*' or '/' between numbers
			if (applyMultiplicationDivisionRule(tokens)) {
				//System.out.println(tokens);
				continue;
			}
			// rule 3: '+' or '-' between numbers
			if (applyAdditionSubtructionRule(tokens)) {
				//System.out.println(tokens);
				continue;
			}
			// rule 4: '(' and ')' around number (also call fix '-')
			if (applyParenthesesRule(tokens)) {
				//System.out.println(tokens);
				continue;
			}

			break;
		}
	}

	private static boolean applyPowerRule(List tokens) {
		// search through all tokens
		for (int i = 0; i < tokens.size(); i++) {
			Object token = tokens.get(i);
			// if '^' sign found
			if ("^".equals(token)) {
				// get surrounding tokens
				Object before = i - 1 < 0 ? null : tokens.get(i - 1);
				Object after = i + 1 >= tokens.size() ? null : tokens.get(i + 1);
				if (before instanceof Double && after instanceof Double) {
					// evaluate '^'
					double result = Math.pow((Double) before, (Double) after);
					// remove 3 tokens
					tokens.remove(i + 1);
					tokens.remove(i);
					tokens.remove(i - 1);
					// add result
					tokens.add(i - 1, result);
					// stop current processing
					return true;
				}
			}
		}

		return false;
	}

	private static boolean applyMultiplicationDivisionRule(List tokens) {
		// invalid tokens surrounding target expression
		List signs = Arrays.asList("^");

		// search through all tokens
		for (int i = 0; i < tokens.size(); i++) {
			Object token = tokens.get(i);
			// if '*' or '/' sign found
			if ("*".equals(token) || "/".equals(token)) {
				// get doubled surrounding tokens
				Object evenBefore = i - 2 < 0 ? null : tokens.get(i - 2);
				Object before = i - 1 < 0 ? null : tokens.get(i - 1);
				Object after = i + 1 >= tokens.size() ? null : tokens.get(i + 1);
				Object evenAfter = i + 2 >= tokens.size() ? null : tokens.get(i + 2);
				// restrict evenBefore token
				List evenBeforeSigns = new ArrayList(Arrays.asList("*", "/"));
				evenBeforeSigns.remove(token);
				if (before instanceof Double && after instanceof Double
						&& !signs.contains(evenBefore) && !signs.contains(evenAfter)
						&& !evenBeforeSigns.contains(evenBefore)) {
					// evaluate '*' or '/'
					double result = 0;
					switch (((String) token).charAt(0)) {
						case '*':
							result = (Double) before * (Double) after;
							break;
						case '/':
							result = (Double) before / (Double) after;
							if (!Double.isFinite(result)) {
								throw new ArithmeticException();
							}
							break;
					}
					// remove 3 tokens
					tokens.remove(i + 1);
					tokens.remove(i);
					tokens.remove(i - 1);
					// add result
					tokens.add(i - 1, result);
					// stop current processing
					return true;
				}
			}
		}

		return false;
	}

	private static boolean applyAdditionSubtructionRule(List tokens) {
		// invalid tokens surrounding target expression
		List signs = Arrays.asList("^", "*", "/");

		// search through all tokens
		for (int i = 0; i < tokens.size(); i++) {
			Object token = tokens.get(i);
			// if '+' or '-' sign found
			if ("+".equals(token) || "-".equals(token)) {
				// get doubled surrounding tokens
				Object evenBefore = i - 2 < 0 ? null : tokens.get(i - 2);
				Object before = i - 1 < 0 ? null : tokens.get(i - 1);
				Object after = i + 1 >= tokens.size() ? null : tokens.get(i + 1);
				Object evenAfter = i + 2 >= tokens.size() ? null : tokens.get(i + 2);
				// restrict evenBefore token
				List evenBeforeSigns = new ArrayList(Arrays.asList("+", "-"));
				evenBeforeSigns.remove(token);
				if (before instanceof Double && after instanceof Double
						&& !signs.contains(evenBefore) && !signs.contains(evenAfter)
						&& !evenBeforeSigns.contains(evenBefore)) {
					// evaluate '+' or '-'
					double result = 0;
					switch (((String) token).charAt(0)) {
						case '+':
							result = (Double) before + (Double) after;
							break;
						case '-':
							result = (Double) before - (Double) after;
							break;
					}
					// remove 3 tokens
					tokens.remove(i + 1);
					tokens.remove(i);
					tokens.remove(i - 1);
					// add result
					tokens.add(i - 1, result);
					// stop current processing
					return true;
				}
			}
		}

		return false;
	}

	private static boolean applyParenthesesRule(List tokens) {
		// search through all tokens
		for (int i = 0; i < tokens.size(); i++) {
			Object token = tokens.get(i);
			// if number found
			if (token instanceof Double) {
				// get surrounding tokens
				Object before = i - 1 < 0 ? null : tokens.get(i - 1);
				Object after = i + 1 >= tokens.size() ? null : tokens.get(i + 1);
				if ("(".equals(before) && ")".equals(after)) {
					// remove parentheses
					tokens.remove(i + 1);
					tokens.remove(i - 1);
					// call '-' sign fixing
					fixNegativeNumbers(tokens);
					// stop current processing
					return true;
				}
			}
		}

		return false;
	}
}

